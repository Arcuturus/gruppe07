#ifndef Gren_hpp
#define Gren_hpp

#include "ListTool2B.h"
#include "ProgFunc.h"
#include "Ovelse.h"
#include "Const.h"
#include "enums.h"

extern List * grener;

class Gren : public TextElement {
private:
	int antovelser, antsifre;
	Ovelse* ovelser[MAXOVELS+1];		//indeks p� alle ovelser i grenen
	moles etter;						//enum om statsu poeng eller tid???

public:
	Gren(char * nvn);
	Gren(ifstream &inn, char* nvn);
	void writeToFile(ofstream &ut);
	void editGren();
    void display();
    void createOvStaList();
    void editOvStaList();
	void createOvResList();
	void showResOvList();
    void printOvStaList();
    void displayOvelser();
	void skrivData();
	void addOvelse();
    void editOvelse();
    void delOvelse();
    void removeOvelse();
	void deleteResOvList();
    void deleteStaOvList();
	void resetOvelse();
    ~Gren();

};

#endif /* Gren_hpp */
