#include "Ovelser.h"

void OSubMenu() {
    cout << "\nListe over valg: " << endl;
    cout << "\tN - Registrer ny ovelse" << endl;
    cout << "\tE - Endre ovelse" << endl;
    cout << "\tF - Fjerne/slette ovelse" << endl;
    cout << "\tL - Lage ny startliste" << endl;
    cout << "\tR - Lage ny resultatslite" << endl;
    cout << "\tA - Skriv hoveddataene om alle ovelser" << endl;
    cout << endl;
}
// Switch for valget bruker gjor i 'O' menyen
void ovelseMeny() {
    char choice;
    choice = read("Valg? ");
    while (choice != 'X') {
        switch (choice) {
            case 'N':
                addOvelse();
                writeToFileG();
                break;
            case 'E':
                editOvelse();
                writeToFileG();
                break;
            case 'F':
                removeOvelse();
                writeToFileG();
                break;
            case 'A':
                display();
                break;
            case 'L': {
                staListSubMeny();
                staListSwitch();
            }break;
			case 'R': {
				resListSubMeny();
				resListSwitch();
			}break;
            default: DSubMenu(); 
                break; 
        }
        OSubMenu();
        choice = read("Valg? ");
    }
    MainMenu();
}

void staListSubMeny() {
    cout << "\nListe over valg:\n"
         << "\tS - Se deltakerliste\n"
         << "\tN - Ny deltakerliste\n"
         << "\tE - Endre deltakerliste\n"
         << "\tF - Fjern deltakerliste\n"
         << "\tX - Tilbake til meny\n\n";
}
// Switch for valget brukeren gjor i 'L' menyen
void staListSwitch() {
    char choice = read("Valg");
    while(choice != 'X') {
        switch (choice) {
            case 'S': { printStartList();  } break;
            case 'N': { createStartList(); } break;
            case 'E': { editStartList();   } break;
            case 'F': { deleteStaList();   } break;
            case 'X':                        break;
            default:                         break;
        }
        choice = read("Valg");
    }
}
// Legg til ovelse i gren
void addOvelse() {
    Gren * gren;
    char * name = newCharPtr("Hvilken gren horer ovelsen til?");
    if(grener->inList(name)) {              // Dersom grenen finnes
        gren = (Gren*)grener->remove(name); // Hent ut i fra listen
        gren->addOvelse();                  // Kall paa funksjon add
        grener->add(gren);                  // Legg tilbake i listen
    }
    else {  // Melding til bruker dersom grenen ikke finnes
        cout << "\n\tDenne grenen finnes ikke!\n";
    }
}

// BEGIN STARTLISTE------------------------------------------------------------
// Opprett en startliste for ovelsen
void createStartList() {
    Gren * gren;
    char * name = newCharPtr("Hvilken gren horer ovelsen til?");
    if(grener->inList(name)) {              // Dersom grenen finnes
        gren = (Gren*)grener->remove(name); // Hent ut fra listen
        gren->createOvStaList();            // Kall paa funksjon create
        grener->add(gren);                  // Legg tilbake i listen
        writeToFileG();                     // Oppdater grenfilen med ant.delt.
    }
    else { // Melding til bruker dersom grenen ikke finnes
        cout << "\n\tFant ikke gren!\n";
    }
}
// Dersom startliste for ovelse finnes, endre paa denne
void editStartList() {
    Gren * gren;
    char * name = newCharPtr("Hvilken gren horer ovelsen til?");
    if(grener->inList(name)) {              // Dersom grenen finnes
        gren = (Gren*)grener->remove(name); // Hent ut fra listen
        gren->editOvStaList();              // Kall paa funksjon edit
        grener->add(gren);                  // Legg tilbake i listen
        writeToFileG();                     // Oppdater grenfilen med endringer
    }
    else {  // Melding til bruker dersom grenen ikke finnes
        cout << "\n\tFant ikke gren!\n";
    }
}
// Skriv ut startlisten til bruker
void printStartList() {
    Gren * gren;
    char * name = newCharPtr("Hvilken gren horer ovelsen til?");
    if(grener->inList(name)) {              // Dersom grenen finnes
        gren = (Gren*)grener->remove(name); // Hent ut fra listen
        gren->printOvStaList();             // Kall pa funksjon print
        grener->add(gren);                  // Legg tilbake i listen
    }
    else {  // Melding til bruker dersom grenen ikke finnes
        cout << "\n\tFant ikke gren!\n";
    }
}

// END STARTLISTE--------------------------------------------------------------

// Looper igjennom alle grener i lista og skriver ut tilhorende ovelser
void display() {
    Gren * gren;
    for(int i = 1; i <= grener->noOfElements(); i++) {
        gren = (Gren*)grener->removeNo(i);
        gren->displayOvelser();
        grener->add(gren);
    }
}

// Endre ovelse i gren
void editOvelse() {
    Gren * gren;
    char * name = newCharPtr("Hvilken gren horer ovelsen til?");
    if(grener->inList(name)) {              // Dersom grenen finnes
        gren = (Gren*)grener->remove(name); // Hent ut fra listen
        gren->editOvelse();                 // Kall paa funksjon
        grener->add(gren);                  // Legg tilbake
    }
    else { // Beskjed til bruker dersom grenen ikke finnes
        cout << "\n\tDenne grener finnes ikke!\n";
    }
}

// Fjern ovelse fra gren
void removeOvelse() {
    Gren * gren;
    char * name = newCharPtr("Hvilken gren horer ovelsen til?");
    if(grener->inList(name)) {              // Dersom grenen finnes
        gren = (Gren*)grener->remove(name); // Hent ut fra listen
        gren->removeOvelse();               // Kall paa funksjon
        grener->add(gren);                  // Legg tilbake i listen
    }
    else { // Beskjed til bruker dersom grenen ikke finnes
        cout << "\n\tDenne grener finnes ikke!\n";
    }
}

// BEGIN RESULTATLISTE---------------------------------------------------------
void resListSubMeny() {
	cout << "\nListe over valg:\n"
		<< "\tS - Se resultatliste\n"
		<< "\tN - Ny resultatliste\n"
		<< "\tF - Fjern resultatliste\n"
		<< "\tX - Tilbake til meny\n\n";
}
// Switch for valget brukeren gjor i 'R' menyen
void resListSwitch() {
	char choice = read("Valg");
	while (choice != 'X') {
		switch (choice) {
		case 'S': { showResList();   } break;
		case 'N': { newResList();    } break;
		case 'F': { deleteResList(); } break;
        default: { resListSubMeny(); } break;
		}
		resListSubMeny();
		choice = read("Valg");
	}
	nullStillOvelse();
	OSubMenu();
}
// Opprett en ny resultatliste
void newResList(){
	char * name = newCharPtr("Hvilken Gren tilhorer ovelsen? "); 
	Gren * gren; 
	if (grener->inList(name)) {             // Dersom grenen finnes
		gren = (Gren*)grener->remove(name); // Hent ut ifra listen
		gren->createOvResList();            // Kall paa funksjon create
		grener->add(gren);                  // Legg tilbake i listen
	}
    else { // Beskjed til bruker dersom grenen ikke finnes
        cout << "\n\tDenne grener finnes ikke!\n";
    }
}
// Vis resultatlisten til bruker
void showResList() {
	char * name = newCharPtr("Hvilken Gren tilhorer ovelsen? ");
	Gren * gren;
	if (grener->inList(name)) {             // Dersom grenen finnes
		gren = (Gren*)grener->remove(name); // Hent ut i fra listen
		gren->showResOvList();              // kall paa funksjon show
		grener->add(gren);                  // Legg tilbake i listen
	}
    else { // Beskjed til bruker dersom grenen ikke finnes
        cout << "\n\tDenne grener finnes ikke!\n";
    }
}

void deleteStaList() {
    char * name = newCharPtr("Hvilken gren tilhorer ovelsen? ");
    Gren * gren;
    if (grener->inList(name)) {             // Dersom grenen finnes
        gren = (Gren*)grener->remove(name); // Hent ut i ifra listen
        gren->deleteStaOvList();            // Kall paa funksjon delete
        grener->add(gren);                  // Legg tilbake i listen
    }
    else { // Beskjed til bruker dersom grenen ikke finnes
        cout << "\n\tDenne grener finnes ikke!\n";
    }
}


void deleteResList() {
	char * name = newCharPtr("Hvilken gren tilhorer ovelsen? ");
	Gren * gren;
	if (grener->inList(name)) {
		gren = (Gren*)grener->remove(name);
		gren->deleteResOvList();
		grener->add(gren);
	}
    else { // Beskjed til bruker dersom grenen ikke finnes
        cout << "\n\tDenne grener finnes ikke!\n";
    }
}

void nullStillOvelse() {
	Gren * gren;
	for (int i = 1; i <= grener->noOfElements(); i++) {
		gren = (Gren*)grener->removeNo(i); 
		gren->resetOvelse();
		grener->add(gren);
	}
}

// END RESULTATLISTE-----------------------------------------------------------
