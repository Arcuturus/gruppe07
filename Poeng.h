#if !defined(__POENG_H)          
#define __POENG_H

#include "Statistikk.h"

class Poeng : public Statistikk {
private:
	char*		plass[MAXNASJONER + 1];
	int			poenger[MAXNASJONER + 1];
	int			antall;
public:
	Poeng();
    Poeng(char * navn);
	Poeng(ifstream &inn);
	void sjekkForsteGang(ifstream &inn);
	void fjernNasjon(int i);
	void sorter();
	void nullstill();
	void writeToFileP();
	void readFromFileP();
	void displayP();
	~Poeng();
};


#endif
