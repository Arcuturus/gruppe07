#include "Medaljer.h" 
#include <cstdio>

//kj�res n�r du tar kommando 'M' leser fra fil og kj�rer display
Medalje::Medalje()
{
	ifstream inn("MEDALJE.DTA");
	
	if (inn) {
		readFromFileM();
	}
	else {
		nullerUt();
	}
	display();
}

//Kj�res n�r du leser inn fra ny res fil, oppdaterer medaljer
Medalje::Medalje(char *navn) {
	char* nasjonfork;
    int paa = 0;
	int trash;
	antNasjon = 0;

	nullerUt();							//Nullstiller og 
	readFromFileM();					//leser fra fil
	ifstream inn(navn);

	if (inn && antNasjon !=0) {			//sjekker om den finner fila
		nasjonfork = newCharPtr(inn);	//og om det liggger nasjoner inne
		inn >> trash; inn.ignore();
		inn >> trash; inn.ignore();
		++paa;
        while(!inn.eof() && paa < 4) {	//looper til eof eller 3 ganger

			bool finnes = false;
										//looper til den finner nasjon
			for (int j = 1; j <= antNasjon; j++) {
				if (!strcmp(nasjonfork,nasjon[j])) {
					finnes = true;
					++medaljer[j][paa];
				}
			}							//vis nasjon ikke finnes legges den til
			if (!finnes) {
				nasjon[++antNasjon] = nasjonfork;
				++medaljer[antNasjon][paa];
			}
			nasjonfork = newCharPtr(inn);
			inn >> trash; inn.ignore();
			inn >> trash; inn.ignore();
            ++paa;
		}
	}						//kj�res kun f�rste gang medalje kj�res
	else if (inn) {			//plusser p� antall og gir medaljer
		sjekkForsteGang(inn);
	}
	else cout << "\nFant ikke filen " << navn << endl;
	sorter();			//sorterer etter medaljer
	skrivTilFil();		//f�r det skrives til fil
}

//kj�res n�r du skal fjerne resultat, oppdaterer medaljer
Medalje::Medalje(ifstream &inn) {
	char* nasjonfork;
	int paa = 0;
	int trash;

	nullerUt();
	readFromFileM();

	if (inn && antNasjon != 0) {
		nasjonfork = newCharPtr(inn);
		inn >> trash; inn.ignore();
		inn >> trash; inn.ignore();
		++paa;
		while (!inn.eof() && paa < 4) {

			for (int j = 1; j <= antNasjon; j++) {
				if (!strcmp(nasjonfork, nasjon[j])) {
					--medaljer[j][paa];
					if (!medaljer[j][1]) {
						if (!medaljer[j][2]) {
							if (!medaljer[j][3]) {
								fjernNasjon(j);
							}
						}
					}
				}
			}
			nasjonfork = newCharPtr(inn);
			inn >> trash; inn.ignore();
			inn >> trash; inn.ignore();
			++paa;
		}
	}
	sorter();
	skrivTilFil();
	if (!antNasjon) {
		remove("MEDALJE.DTA");
	}
}

//fjerner nasjon i fra lista
void Medalje::fjernNasjon(int i) {
	for (int j = i; j < antNasjon; j++) {
		nasjon[j] = nasjon[j + 1];
		for (int k = 1; k <= 3; k++) {
			medaljer[j][k] = medaljer[j + 1][k];
		}
	}
	nasjon[antNasjon] = nullptr;
	for (int j = 1; j <= 3; j++) {
		medaljer[antNasjon][j] = 0;
	}
	--antNasjon;
}

void Medalje::display()
{
	for (int i = 1; i <= antNasjon; i++) {
		cout << endl << nasjon[i];
		for (int j = 1; j <= 3; j++) {
			if (j == 1) {
				cout << "\tGULL: " << medaljer[i][j];
			}
			else if (j == 2) {
				cout << "\tSOLV: " << medaljer[i][j];
			}

			else {
				cout << "\tBRONSE: " << medaljer[i][j] << endl;
			}
		}
	}

}

void Medalje::readFromFileM() {
	ifstream inn("MEDALJE.DTA");
	char* nationName;

	if (inn) {
		inn >> antNasjon; inn.ignore();
		for (int i = 1; i <= antNasjon; i++) {
			nationName = newCharPtr(inn);
			nasjon[i] = nationName;
			for (int j = 1; j <= 3; j++) {
				int m;
				inn >> m;
				medaljer[i][j] = m;
			}
			inn.ignore();
		}
	}
	else cout << "\nFant ikke filen 'MEDALJER.DTA'" << endl;
}

void Medalje::skrivTilFil(){
	ofstream ut("MEDALJE.DTA");

	ut << antNasjon << endl;

	for (int i = 1; i <= antNasjon; i++) {
		ut << nasjon[i] << endl;
		for (int j = 1; j <= 3; j++) {
			if (j == 1) {
				ut << "\t" << medaljer[i][j];
			}
			else if (j == 2) {
				ut << "\t" << medaljer[i][j];
			}

			else {
				ut << "\t" << medaljer[i][j] << endl;
			}
		}
	}
}

void Medalje::sorter() {
	int j;
	for (int i = 1; i <= antNasjon; i++) {
		j = i;					//hvis de har like mange gul sjekkes alt
		while (medaljer[j][1] >= medaljer[j - 1][1] && j > 1) {
			if (medaljer[j][1]==medaljer[j-1][1] && sjekkAlt(j)) {
				byttPlass(j);
			}
			else if (medaljer[j][1]>medaljer[j-1][1]) { 
				byttPlass(j); 
			}
			--j;
		}
	}
}

void Medalje::byttPlass(int j) {
	int temp;
	char * temper;

	temper = nasjon[j];
	nasjon[j] = nasjon[j - 1];
	nasjon[j - 1] = temper;
	for (int i = 1; i <= 3; i++) {
		temp = medaljer[j][i];
		medaljer[j][i] = medaljer[j - 1][i];
		medaljer[j - 1][i] = temp;
	}
}

//sjekker s�lv bronse s� pong returnerer 1 vis de skal bytte plass
bool Medalje::sjekkAlt(int j) {
	if (medaljer[j][2] == medaljer[j - 1][2]) {
		if (medaljer[j][3] == medaljer[j - 1][3]) {
			return(mestPoeng(nasjon[j], nasjon[j - 1]));
		}
		else return (medaljer[j][3] > medaljer[j - 1][3]);
	}
	else return(medaljer[j][2] > medaljer[j - 1][2]);
}

void Medalje::nullerUt() {
	antNasjon = 0;
	for (int i = 1; i <= MAXNASJONER; i++) {
		nasjon[i] = nullptr;
		for (int j = 1; j <= 3; j++) {
			medaljer[i][j] = 0;
		}
	}
}

//g�r kun f�rste gang det leses inn for medalje!!
void Medalje::sjekkForsteGang(ifstream &inn) {
	int trash;
	int pla = 0;
	char * navn;
	bool sjekk = false;
	
	nasjon[++antNasjon] = newCharPtr(inn);
	++medaljer[antNasjon][antNasjon];
	inn >> trash; inn.ignore();
	inn >> trash; inn.ignore();
	++pla;
	while (!inn.eof() && pla < 3) {
		navn = newCharPtr(inn);
		inn >> trash; inn.ignore();
		inn >> trash; inn.ignore();
		++pla;
		for (int i = 1; i <= antNasjon; i++) {
			if (!strcmp(nasjon[i], navn)) {
				sjekk = true;
				++medaljer[i][pla];
			}
		}
		if (!sjekk) {
			nasjon[++antNasjon] = navn;
			++medaljer[antNasjon][pla];
		}
	}
}

Medalje::~Medalje() {
	for (int i = 1; i <= antNasjon; i++) {
        nasjon[i] = nullptr;
	}
}
