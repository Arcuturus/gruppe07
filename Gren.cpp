#include "Gren.h"
#include "iostream"


Gren::Gren(char * nvn) : TextElement(nvn) {
	char valg = read("Moles grenen etter poeng eller tid?(P/T)?");

	while (valg != 'P' && valg != 'T') {
		valg = read("Moles grenen etter poeng eller tid?(P/T)?");
	}
	(valg == 'P')? etter = poeng : etter = tid; //setter enum til 0 eller 1
	
	for (int i = 1; i <= MAXOVELS; i++) {	//setter de til � peke p�
		ovelser[i] = nullptr;				//null pointer
	}

    antsifre = grener->noOfElements()+1;
	antovelser = 0;
}

Gren::Gren(ifstream &inn, char* nvn) : TextElement(nvn){
	int buffer;

	inn >> buffer; inn.ignore();
	(buffer) ? etter = tid : etter = poeng;
	inn >> antovelser; inn.ignore();
	inn >> antsifre; inn.ignore();

	for (int i = 1; i <= antovelser; i++) {
		if (i <= antovelser) {
			ovelser[i] = new Ovelse(inn); 
		}
		else ovelser[i] = nullptr;
	}
}

void Gren::display() {
    cout << text;
}

void Gren::createOvStaList() {											//Creates a startlist for a single event
    bool exists = false;
    if(antovelser == 0) {												//If there are no events 
        cout << "\n\tIngen ovelser i gren!\n";
        exists = true;
    }
    else {
        char name[TXT];
        read("\n\tNavn pa ovelse", name, TXT);
		capitalize(name); 
        for(int i = 1; i <= antovelser; i++) {							//Loop through events
            if(!ovelser[i]->finnes(name)) {								//Check if event exists
                exists = true;
                ovelser[i]->createStartList();							//Create startlist on that single event
            }
        }
    }
    if(!exists) cout << "\n\tFinner ikke ovelse!\n";
}

void Gren::editOvStaList() {											//Edit startlist for a single event
    bool exists = false;
    if(antovelser == 0) {												//If there are no events
        cout << "\n\tIngen ovelser i gren!\n";
        exists = true;
    }
    else {
        char name[TXT];
        read("\n\tNavn pa ovelse", name, TXT);
        for(int i = 1; i <= antovelser; i++) {							//Loops through all events
            if(!ovelser[i]->finnes(name)) {								//if event exists
                exists = true;
                ovelser[i]->changeStartList();							//change the start list of that single event
            }
        }
    }
    if(!exists) cout << "\n\tFinner ikke ovelse!\n";
}

void Gren::printOvStaList() {											//Print the startlist of a single event
    bool exists = false;
    if(antovelser == 0) {												//If there are no events
        cout << "\n\tIngen ovelser i gren!\n";
        exists = true;
    }
    else {
        char name[TXT];
        read("\n\tNavn pa ovelse", name, TXT);
        for(int i = 1; i <= antovelser; i++) {
            if(!ovelser[i]->finnes(name)) {								//If event exists
                exists = true;
                ovelser[i]->printStartList();							//Print the startlist of that single event
            }
        }
    }
    if(!exists) cout << "\n\tFinner ikke ovelse!\n";
}

void Gren::editGren() {
	text = newCharPtr("Hva vil du forandre til?");

	while (grener->inList(text)) {
		text = newCharPtr("Grenen finnes!\nHva vil du forandre til?");
	}
}

void Gren::skrivData() {
    cout << endl << antsifre << '\t' << text ;
	cout << endl << "Grenen moles etter "; 
	(etter) ? cout << "tid" : cout << "poeng";
	cout << "\nDet er " << antovelser << ".- ovelser i grenen" << endl << endl;
	
	for (int i = 1; i <= antovelser; i++) {
		ovelser[i]->display();
	}
}

void Gren::writeToFile(ofstream &ut) {
	ut << text << '\n' << etter << '\n'
       << antovelser << '\n' << antsifre << '\n';
	for (int i = 1; i <= antovelser; i++) {
		ovelser[i]->writeToFile(ut);
	}
}

void Gren::addOvelse() {
    bool exists = false;
    if(antovelser < MAXOVELS) {
        char name[TXT];
        read("\n\tNavn pa ovelse", name, TXT);
        if(antovelser == 0) {
            ++antovelser;
            int uniq = antsifre * 100 + antovelser;
            ovelser[antovelser] = new Ovelse(name, uniq);
        }
        else {
            for(int i = 1; i <= antovelser; i++) {
                if(!ovelser[i]->finnes(name)) {
                    exists = true;
                }
            }
            if(!exists) {
                ++antovelser;
                int uniq = antsifre * 100 + antovelser;
                ovelser[antovelser] = new Ovelse(name, uniq);
            }
            else {
                cout << "\n\tDenne ovelsen finnes allerede!\n";
            }
        }
    }
    else {
        cout << "\n\tFullt med ovelser!\n"
             << "\tFor aa kunne legge til en ny ovelse\n"
             << "\rEr du nodt til aa slette en forst!\n";
    }
}

void Gren::delOvelse() {
    bool exists = false;
    if(antovelser == 0) {
        cout << "\n\tIngen ovelser i gren!\n";
        exists = true;
    }
    else {
        char name[TXT];
        read("\n\tNavn pa ovelse", name, TXT);
        for(int i = 1; i <= antovelser; i++) {  // Loop paa alle ovelser
            if(!ovelser[i]->finnes(name)) {     // Sjekk om ovelse finnes
                exists = true;                  // Ovelse eksisterer
                ovelser[i]->removeSelf();       // Sletter sine egne data
                ovelser[i] = nullptr;           // Til nullptr for minne
                // TODO Sortere array           // Sortere array
            }
        }
    }
    if(!exists) cout << "\n\tFinner ikke ovelse\n";
}

void Gren::editOvelse() {
    char name[TXT];
    read("\n\tNavn pa ovelse", name, TXT);
    if(antovelser != 0) {
        for(int i = 1; i <= antovelser; i++) {
           if(!ovelser[i]->finnes(name)) {
               cout << "\n\tHva onsker du aa endre paa?\n"
                    << "\tN - Endre navn\n" << "\tD - Endre dato\n"
                    << "\tT - Endre starttid\n";
               char valg = read("\n\tDitt valg");
               ovelser[i]->edit(valg);
           }
        }
        
    }
    else cout << "\n\tIngen ovelser i gren\n";
}

void Gren::displayOvelser() {
    if(antovelser > 0) {
        display();
        for(int i = 1; i <= antovelser; i++) {
            cout << "\nOVELSER\n";
            ovelser[i]->display();
        }
    }
}

void Gren::createOvResList() {													//Creates a resultlist for an event
	char * name = newCharPtr("I hvilken ovelse vil du legge inn resultater?");
	for (int i = 1; i <= antovelser; i++) {										//Loops through events
		if (!ovelser[i]->finnes(name)) {										//Checks that event exists
			ovelser[i]->createResultList(etter);								//Create resultlist on the event
		}
	}
}

void Gren::removeOvelse() {														//remove entire event
    char * name = newCharPtr("Hvilken ovelse vil du slette?\n\tNavn");
    for (int i = 1; i <= antovelser; i++) {										//Loops through all events
        if (!ovelser[i]->finnes(name)) {										//if the event exists
            if(ovelser[i]->removeSelf()) {										//remove the first event in the array
                ovelser[i] = ovelser[antovelser];								//The one to be deleted = the last one
                ovelser[antovelser] = nullptr;									//The last one = nullpointer
                cout << "\n\tOvelse slettet!\n";
                antovelser--;													//reduce number of events
            }
            else {
                cout << "\n\tKunne ikke slette ovelse!\n";
            }
        }
    }
}

void Gren::showResOvList() {												   	 //Display/read the resultlist of an event
	char * name = newCharPtr("Hvilken ovelse vil se resultatlisten til? ");
	for (int i = 1; i <= antovelser; i++) {										 //Loops through events
		if (!ovelser[i]->finnes(name)) {										 //If event exists
			ovelser[i]->showResultList(etter);									 //Show resultlist for this event
		}

	}
}

void Gren::deleteResOvList() {													 //Delete the resultlist of an event
	char * name = newCharPtr("Hvilken ovelse vil slette resultatlisten til? ");
	for (int i = 1; i <= antovelser; i++) {										 //Loops through events
		if (!ovelser[i]->finnes(name)) {										 //If event exists
			ovelser[i]->deleteResultList();									     //Delete the resultlist
		}

	}
}

void Gren::deleteStaOvList() {													//Delete startlist of an event
    char * name = newCharPtr("Hvilken ovelse vil slette resultatlisten til? ");
    for (int i = 1; i <= antovelser; i++) {										//Loops through events
        if (!ovelser[i]->finnes(name)) {										//If events exists
            ovelser[i]->deleteStartList();										//Delete the start list
        }
    }
}

<<<<<<< HEAD

void Gren::resetOvelse() {
	for (int i = 1; i <= antovelser; i++) {
		ovelser[i]->nullStill();
	}
}
=======
Gren::~Gren() { // Nullstill alle ovelsene til gren
    for(int i = 1; i <= MAXOVELS; i++) {
        ovelser[i] = nullptr;
    }
}
>>>>>>> 8680a910387e67352247b1b2cbb33aaf37d96cfb
