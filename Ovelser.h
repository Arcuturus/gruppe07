#ifndef Ovelser_hpp
#define Ovelser_hpp

#include <stdio.h>
#include "ProgFunc.h"
#include "Ovelse.h"
#include "Grener.h"

extern List * grener;

void OSubMenu();                            // Print meny for O funksjon
void ovelseMeny();                          // Switch for O funksjon
void staListSubMeny();                      // Print meny for L funksjon
void staListSwitch();                       // Switch for L funksjon
void createStartList();                     // Opprett startliste
void editStartList();                       // Rediger startliste
void printStartList();                      // Vis startliste
void resListSubMeny();                      // Print meny for R funksjon
void display();                             // Vis alle grener
void addOvelse();                           // Legg til ovelse i gren
void editOvelse();                          // Rediger ovelse i gren
void removeOvelse();                        // Fjern ovelse fra gren
void resListSwitch();                       // Switch for R funksjon
void showResList();                         // Vis resultatliste
void newResList();                          // Opprett resultatliste
void deleteResList();                       // Fjern resultatliste
void deleteStaList();                       // Fjern startliste
void nullStillOvelse();

#endif /* Ovelser_hpp */
