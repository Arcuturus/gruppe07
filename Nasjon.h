#if !defined(__NASJON_H)          
#define __NASJON_H   

#include "ListTool2B.h"
#include "ProgFunc.h"


using namespace std; 

class Nasjon : public TextElement {
private:
    char * navn;
    int    antDelt;
    char * kontaktPerson;
    int    kontaktTelefon;
public:
	Nasjon(const char * n);                   // Create new nation
    Nasjon(const char * n, ifstream & f);     // Create nation from file
    void display();                           // Display all nations in list
    void skrivTilFil(ofstream & f);           // Write all nations to file
	void outName(ofstream & f);               // Prints the 'XXX' name to file
    char * getName();                         // Get nations name
    bool edit();                              // Edit nation
	void antDeltInc(){antDelt++;}             // Increment no of contestants
    ~Nasjon();
};

#endif
