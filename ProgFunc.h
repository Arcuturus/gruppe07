#if !defined(__PROGFUNC_H)
#define __PROGFUNC_H

#include <stdio.h>
#include <iostream>
#include <cstring>
#include <fstream>

#include "enums.h"
#include "Const.h"

using namespace std;

#endif /* ProgFunc_hpp */

void MainMenu();
void RSubMenu();
void LSubMenu();
void GSubMenu();
void DSubMenu();

//Competitor functions

void read(const char* t, char* s, const int LEN);   //reads char pointer
int read(const char* t, int min, int max);	        //reads min/max value
float read(const char* t, float min, float max);	//reads FLOAT min/max value 
char read(const char* t);							//Returns upcased char
char * newCharPtr(const char* info);				//creates a new char pointer without wasted space
void nationSHORT(char * name);                      //SHORT NATION SHIT
char * allcaps(char * string);                      //Returns upcased string
char * capitalize(char * string);                   //Capitalizes word
char* newCharPtr(ifstream & inn);                   //Reads line from file
bool checkDate(int dato);                           //Check if legal date
int retDate(const char * t, int MIN, int MAX);      //Return legal date
bool checkTime(int time);                           //Check if legal time
int retTime(const char * t, int MIN, int MAX);      //Return legal time
