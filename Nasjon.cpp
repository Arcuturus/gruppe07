#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#endif

#include "Nasjon.h"
// Opprett en ny nasjon
Nasjon::Nasjon(const char* n) : TextElement(n) {
    antDelt = 0;                                        // Reset ant delt.
    navn = capitalize(newCharPtr("Landets navn: "));    // Les navnet
    kontaktPerson = newCharPtr("Kontaktperson: ");      // Les kontaktperson
    kontaktTelefon = read("Telefonnummer: ", TLFMIN, TLFMAX); // Les tlf
    cout << "\t\nNasjon " << text <<  " opprettet!\n"; // Melding om ok
}
// Opprett en ny nasjon fra fil
Nasjon::Nasjon(const char * n, ifstream & f) : TextElement(n) {
    navn = newCharPtr(f);               // Navnet leses fra fil (egen funk)
    kontaktPerson = newCharPtr(f);      // Kontaktperson leses fra fil
    f >> kontaktTelefon; f.ignore(2);   // Les telefonnummer og ignorer 2
}
// Skriv alll informasjon om nasjonen ut til konsoll
void Nasjon::display() {
    cout << "\nNasjon: " << text << '\t' << navn << '\t'
         << "Kontaktperson: " << kontaktPerson
         << ", " << kontaktTelefon;
}
// Endre pa innholdet i en nasjon
bool Nasjon::edit() {
    try { // Prov aa endre paa en nasjon
        cout << "\n\tHva ønsker du å endre på?: "
             << "\n\tN - Nasjonsnavn"
             << "\n\tK - Kontaktperson"
             << "\n\tT - Kontakttelefon"
             << "\n\tA - Avslutt uten å endre";
        
        char choice = read("\n\tDitt valg('a' for avslutt)");
        
        while(choice != 'A') { // Loop saa lenge valg ikke er A
            switch (choice) {
                case 'N': { // Spor om nytt navn paa land
                    navn = capitalize(newCharPtr("Landets navn: "));
                } break;
                case 'K': { // Endre kontaktperson
                    char buffer[TXT];
                    read("Kontaktperson: ", buffer, TXT);
                    kontaktPerson = new char[strlen(buffer)]; //Bruke funksjon?? 
	                strcpy(kontaktPerson, buffer);
                } break;
                case 'T': { // Endre kontakttelefon
                    kontaktTelefon = read("Telefonnummer: ", TLFMIN, TLFMAX);
                } break;
                default: break;
            } // Melding til bruker om at endringer er lagret
            cout << "\n\tEndring lagret!\n";
            choice = read("\n\tDitt valg('a' for avslutt)");
        }
        return true; // Endring OK, returner true
    }
    catch(int n) { // Endring IKKE ok. Hent feilmeldingsnummer
        cout << "ERROR: " << n << '\n'          // Skriv nummer til bruker
             << "Could not change country!\n";  // Melding om IKKE ok
        return false;                           // Returner false
    }
}
// Skriv en hel nasjon til fil
void Nasjon::skrivTilFil(ofstream & f) {
    f << text << '\n' << navn << '\n'
      << kontaktPerson << '\n' << kontaktTelefon << "\n\n";
}
// Skriv nasjonsforkortelse til fil
void Nasjon::outName(ofstream & f) {
	f << text << endl; 
}
// Returnerer nasjonens forkortelse
char * Nasjon::getName() {
    return text;
}

Nasjon::~Nasjon() {
    delete navn;
    delete kontaktPerson;
}
