#if !defined(__OVELSE_H)
#define __OVELSE_H

#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#endif


#include <stdio.h>
#include "ListTool2B.h"
#include "ProgFunc.h"
#include "Deltaker.h"

class Ovelse {
private:
    int    id;               // Unik ID-nummer
    char * navn;             // Navn paa ovelse
    int    klokkeslett;      // Klokkeslett for start;
    int    dato;             // Dato for start;
    int    antDelt;          // Antall deltakere
	int	   antRes;			 // Antall resultat registrert
    int startTime[MAXDELT + 1];
    int startNum[MAXDELT + 1];
    Deltaker * deltakerListe[MAXDELT+1];
	float results[MAXDELT + 1];  //Liste for resultater
    
public:
    Ovelse(const char * name, int uniq);          // Opprett ovelse
	Ovelse(ifstream &inn);                        // Opprett ovelse fra fil
    void edit(char valg);                         // Rediger ovelse
	void display();                               // Vis ovelse
    bool finnes(const char * name);               // Sjekk om ovelse finnes
    void createStartList();                       // Lag startliste for ovelse
    void writeToStartList(const char * filename); // Skriv til startlisten
    void changeStartList();                       // Endre paa startlisten
    void updateStartList(const char * f);         // Oppdater endringer sta.li.
    void deleteDeltaker(int i);                   // Fjern deltaker fra listen
    void changeStartNumber(int i);                // Endre startnummer delt.
    void readStartList(ifstream &f);              // Les fra startlisten
	void createResultList(moles etter);           // Lag resultatliste
	void showResultList(moles etter);             // Vis resultatliste
	void sortResultList(moles etter);             // Sorter resultatlisten
	void sort(int i);                             // Sorter helpefunksjon
	void displayResList(moles etter);             // Vis resultatlisten
    void printStartList();                        // Vis startlisiten
    char * createFileName(const char * ext);      // Lag tilhorende filnavn
	void writeToFile(ofstream &ut);               // Skriv ovelse til fil
    void registrerPoeng(moles etter);			  // Registrers resultlist after points
    void registrerTid(moles etter);				  // Registrers resultlist after time
	void writeResToFile();                        // Skriv resultat til fil
	void writeContestantToFile();                 // Skriv startliste til fil
	void deleteResultList();                      // Fjern resultatliste
    void deleteStartList();                       // Fjern startliste
	void nullStill();							  // Resets the arrays in use for resultlists & startlists
    bool removeSelf();                            // Fjern dette objektet
    ~Ovelse();                                    // Destructor
};

#endif /* OVELSE_H */
