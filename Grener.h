//
//  Grener.hpp
//  Prosjekt
//
//  Created by Anders on 23/03/2017.
//  Copyright © 2017 Anders. All rights reserved.
//

#ifndef Grener_hpp
#define Grener_hpp

#include "Gren.h"


//GRENER DEKLARASJONER---------------------------------------------------------

void grenSwitch();
void newGren();
void changeGren();
void displayAllGren();
void displayOne();
void writeToFileG();
void readFromFileG();

//END GRENER-------------------------------------------------------------------


#endif /* Grener_hpp */
