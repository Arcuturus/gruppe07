//
//  Nasjoner.h
//  Prosjekt
//
//  Created by Anders on 23/03/2017.
//  Copyright © 2017 Anders. All rights reserved.
//

#ifndef Nasjoner_hpp
#define Nasjoner_hpp

#include <stdio.h>

#include "ListTool2B.h"
#include "Nasjon.h"
#include "Deltaker.h"
#include "ProgFunc.h"

// Fetch the list from main (change this?)
extern List * nasjoner;

//NATION FUNCTION DECLARATIONS-------------------------------------------------

void NSubMenu();                        // Print meny for N choice
void newNation();                       // Create a new nation
void nationMenu();                      // Switch meny for N
void writeContestants();                // Write all of the nations contestants
void editNation();                      // Edit the nation
void displayAllNations();               // Display all nations in list
void infoNation();                      // Get info about ONE nation
void nationReadFromFile();              // Read all nations from file
void nationWriteToFile();               // Write new nations or changes to file

//END NATION FUNCTION DECLARATIONS---------------------------------------------

#endif /* Nasjoner_h */
