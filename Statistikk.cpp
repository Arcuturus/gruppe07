#include "Statistikk.h"



Statistikk::Statistikk() {
	ifstream inn("POENG.DTA");

	if (inn) {
		readFromFileStat();
	}
	else {
		antfork = 0;
		for (int i = 1; i <= MAXNASJONER; i++) {
			fork[i] = nullptr;
		}
	}
}

bool Statistikk::mestPoeng(char*en, char*to) {
	int i = 1;
	int j = 1;
	
	while (strcmp(en, fork[i])) {
		++i;
	}
	while (strcmp(to, fork[j])) {
		++j;
	}
	return (en>to);
}

void Statistikk::readFromFileStat() {
	ifstream inn("POENG.DTA");
	char buffer[TXT];

	inn >> antfork; inn.ignore();
	for (int i = 1; i <= antfork; i++) {
		fork[i] = newCharPtr(inn);
		inn.getline(buffer, TXT);
	}
}
