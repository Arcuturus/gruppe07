//
//  Grener.cpp
//  Prosjekt
//
//  Created by Anders on 23/03/2017.
//  Copyright © 2017 Anders. All rights reserved.
//

#include "Grener.h"
#include "iostream"
#include "ProgFunc.h"


void GSubMenu() {
	cout << "\nListe over valg: " << endl;
	cout << "\tN - Registrer ny gren" << endl;
	cout << "\tE - Endre gren" << endl;
	cout << "\tA - Skriv hoveddataene om alle grener" << endl;
	cout << "\tS - Skriv alle data om en gitt gren" << endl;
	cout << "\tX - Quit" << endl;
	cout << endl;
}

void grenSwitch() {
	char valg = read("Hva vil du gjøre?: ");

	while(valg != 'X'){
		switch (valg) {
            case 'N': {
                newGren();
                writeToFileG();
            }break;
                
            case 'E': {
                changeGren();
                writeToFileG();
            }break;
            
            case 'A': {
                displayAllGren();
            }break;
		
            case 'S': {
                displayOne();
            }break;
		}
		GSubMenu();
		valg = read("Hva vil du gjøre?: ");
	}
	MainMenu();
}

void newGren() {
	Gren* nygren;
	char* navn = capitalize(newCharPtr("Hva heter grenen?: "));

	if (!grener->inList(navn)) {
		nygren = (Gren*)new Gren(navn);
		grener->add(nygren);
	}
	else cout << "\nGrenen finnes allerede din dust...";
}

void changeGren() {
	Gren* sjekk;
	char* gren = capitalize(newCharPtr("Hvilken gren vil du endre?"));

	if (grener->inList(gren)) {
		sjekk = (Gren*)grener->remove(gren);
		sjekk->editGren();
		grener->add(sjekk);
	}
	else cout << "\nFant ikke Grenen...";
}

void displayAllGren() {
	int loop = grener->noOfElements();
	Gren* hent;

	for (int i = 1; i <= loop; i++) {
		hent = (Gren*)grener->removeNo(i);
		hent->skrivData();
		grener->add(hent);
	}
}

void displayOne() {
	char* heter = capitalize(newCharPtr("Hva heter grenen du vil sjekke?: "));
	Gren* sjekk;

	if (grener->inList(heter)) {
		sjekk = (Gren*)grener->remove(heter);
		sjekk->skrivData();
		grener->add(sjekk);
	}
	else cout << "\nFant ikke Gren!!!";
}

void writeToFileG() {
	ofstream ut("GRENER.DTA");
	int loop = grener->noOfElements();
	Gren* write;

	ut << loop << '\n';		//skriver hvor mange grener det er først
	for (int i = 1; i <= loop; i++) {	//går igjennom alle grener og kaller
		write = (Gren*)grener->removeNo(i);	//på skriv funksjon til hver gren
		write->writeToFile(ut);
		grener->add(write);
	}
}

void readFromFileG() {
	ifstream inn("GRENER.DTA");
	if (inn) {
		cout << "Leser fra filen 'GRENER.DTA'" << endl;
		Gren* buffer;
		int loop;
        inn >> loop; inn.ignore();
		for (int i = 1; i <= loop; i++) {
			buffer = new Gren(inn, newCharPtr(inn));
			grener->add(buffer);
		}
	}
	else {
		cout << "Filen 'GRENER.DTA' finnes ikke" << endl;
	}
}
