#if !defined(__STATISTIKK_H)
#define __STATISTIKK_H

#include "ProgFunc.h"

class Statistikk {
private:
	char * fork[MAXDELT + 1];
	int antfork;
public:
    Statistikk();                   // Oppretter statistikk objekt
	void readFromFileStat();        // Les statistikk fra fil
	bool mestPoeng(char*en,char*to);// Sjekker hvem som har mest poeng
};

#endif /* __STATISTIKK_H */
