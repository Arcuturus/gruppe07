//
//  Deltakere.hpp
//  Prosjekt
//
//  Created by Anders on 23/03/2017.
//  Copyright © 2017 Anders. All rights reserved.
//

#if !defined(__DELTAKERE_H)          
#define __DELTAKERE_H

#include <stdio.h>

#include "ListTool2B.h"
#include "Deltaker.h"
#include "ProgFunc.h"

extern List * deltakere;

//DELTAKER FUNCTION DECLARATIONS-----------------------------------------------
void contestantMenu();								//Switch over different choices
void newContestant();								//Create new contestant
void editContestant();								//Edit contestant
void printSingleData();								//Print data of single contestant
void printAllData();								//Print the whole contestant-list
void deltakerSkrivTilFil();							//Print to file
void deltakerLesFraFil();							//Read from file
void editMenu();									//Menu for editing choices
//-----------------------------------------------------------------------------
#endif /* __DELTAKERE_H */
