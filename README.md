# README #
I denne filen finner du:

 1. Kontaktinformasjon
 2. Prosjektinformasjon
 3. Hvordan kjøre

## Kontaktinformasjon ##

| **Navn**              | **Telefon**   | **E-post**         |
| --------------------- |:--------:| ---------------------:  |
| Anders Isaksen        | 91781932 | anderisa@stud.ntnu.no   |
| Varand Rebni Kjer     | 97009690 | varanrk@stud.ntnu.no    |
| Vegard Elshaug Almaas | 90168216 | vegardea@stud.ntnu.no   |

## Prosjektinformasjon ##

## Hvordan kjøre programmet ##