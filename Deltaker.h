#if !defined(__DELTAKER_H)          
#define __DELTAKER_H

#include <iostream>

#include "ListTool2B.h"
#include "ProgFunc.h"
#include "Nasjon.h"
#include "Nasjoner.h"
#include "Deltakere.h"

using namespace std;

extern List * nasjoner; //Declare already-declared list(extern), to access it

class Deltaker : public NumElement {
private:
	char * name;
	kjonn kjonn;
	Nasjon * nasjon;
	int vekt;
	float hoyde;
public:
	Deltaker(int nr);						//constructor
	Deltaker(ifstream & inn, int nr);		//read-from-file constructor
	void skrivTilFil(ofstream & ut);		//print to file
	void edit();							//edit contestant
	void gender();							//(enum) gender
    bool nationality(const char * name);    //returns nationality
	void addNation();						//Add nation for contestant
	void display();							//display information (to screen)
	Nasjon* getNasj() { return nasjon; }
	int getID() { return number; }
    char * getName() { return name; }
    char * getNat() {return nasjon->getName(); }
    ~Deltaker();
};

#endif
