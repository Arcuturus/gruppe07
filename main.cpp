#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#endif 

#include "Statistikk.h"
#include "Nasjoner.h"
#include "Deltakere.h"
#include "Grener.h"
#include "Ovelser.h"
#include "Medaljer.h"
#include "Poeng.h"

List * nasjoner  = new List(Sorted);
List * deltakere = new List(Sorted);
List * grener    = new List(Sorted);

int main() {
    
    nationReadFromFile();  // Nasjoner read from file
    deltakerLesFraFil();   // Deltakere read from file
    readFromFileG();       // Leser grener fra fil

	char choice;           // Holds user choice
    
	MainMenu();

	choice = read("Valg");
	while (choice != 'X') {
		switch (choice) {
            case 'N': {
                NSubMenu();
                nationMenu();
            }
			break;
			case 'D': {
				DSubMenu();
				contestantMenu();
			}
			break;
            case 'G': {
                GSubMenu();
                grenSwitch();
            }
			break;
            case 'O': {
                OSubMenu();
				ovelseMeny();
            }
			break;
			case 'M': {
				Medalje * med;
				med = new Medalje();
				delete med;
		}
			break;
			case 'P': {
				Poeng * pong;
				pong = new Poeng();
				delete pong;
		}
			
			break;
		default:
            MainMenu();
			break;
		}
		choice = read("Valg ");
	};

	cout << "\nAvslutter program\n";

	return 0;
}
