//
//  ProgFunc.cpp
//  Prosjekt
//
//  Created by Anders on 19/03/2017.
//  Copyright © 2017 Anders. All rights reserved.
//

#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#endif

#include <iostream>
#include "ProgFunc.h"

//READ-FUNCTIONS---------------------------------------------------------------
void read(const char* t, char* s, const int LEN) {	 //Displays a string
    do {											 //reads a string from user
        cout << '\t' << t << ": ";	cin.getline(s, LEN);
       while(strlen(s) > LEN) {
            cout << '\t' << t << ": ";	cin.getline(s, LEN);
        }
    } while (strlen(s) == 0);
}

int read(const char* t, int min, int max) {               //Displays a string
    int tall;											  //And reads min and

	do {												  //max value from user
        cout << '\t' << t << " (" << min << '-' << max << "):  ";
		cin >> tall;	cin.ignore(); 
 } while (tall < min || tall > max);


	return tall;
}

float read(const char* t, float min, float max) {         //Displays a string
	float tall;											  //And reads min and
	do {												  //max value from user
		cout << '\t' << t << " (" << min << '-' << max << "):  ";
		cin >> tall;  cin.ignore();
	} while (tall < min || tall > max);
	return tall;
}

char read(const char* t) {								 //returns upcased char
    cin.clear();
    char c;
    cout << "\n" << t << ": ";
    cin >> c; cin.ignore();
    return (toupper(c));
}

char* newCharPtr(const char* info) {
    char buffer[TXT];						   //stores away char array
    read(info, buffer, TXT);			       //gets userinput
    char* n;								   //creates a char pointer
    n = new char[strlen(buffer) + 1];	       //Decides pointer & lenght
    strcpy(n, buffer);						   //copies string from buffer to n
    return capitalize(n);								   //returns the char pointer
}

// Converts the whole string to uppercase
char * allcaps(char * string) {
    for(int i =0; i <= strlen(string); i++) {
        string[i] = toupper(string[i]);
    }
    return string;
}
// Capitalizes the first letter in the word
char * capitalize(char * string) {
    for(int i = 0; i <= strlen(string); i++) {
        if(i == 0) string[0] = toupper(string[0]);
        else {
            string[i] = tolower(string[i]);
        }
    }
    return string;
}

//Bruke denne nÂr lese ny char ptr fra fil?
char* newCharPtr(ifstream & inn) {			       //takes file as argument
	char buffer[TXT];								   //Stores away char array
	inn.getline(buffer, TXT);						 //reads a string from file
	char* n;										   //creates a char pointer
	n = new char[strlen(buffer) + 1];				 //Decides pointer & length
	strcpy(n, buffer);						   //copies string from buffer to n
	return n;										 //returns the char pointer
 }

// Tar imot nasjonsforkortelse og sjekker om lengden er innenfor
void nationSHORT(char * name) {
	char buffer[TXT];
	read("Nasjonsforkortelse? ", buffer, TXT);
	while (strlen(buffer) != 3) {
		cout << "IKKE LOV MED MER ENN 3 BOKSTAVER" << endl;
		read("Nasjonsforkortelse? ", buffer, TXT);
	}
	allcaps(strcpy(name, buffer));
}

// Returnerer dato
int retDate(const char * t, int MIN, int MAX) {
    int input;                          // Holder datoinput
    cout << t; cin >> input;            // Skriv teskt og les dato
    while(input > MAX || input < MIN) { // Sjekk forst om lovig datolengde
        while(!checkDate(input)) {      // Sjekk om lovlig dato
            cout << t; cin >> input;    // Spor igjen dersom ikke lovig dato
        }
    } return input;                     // Returner dato
}

// Returnerer tidspunkt
int retTime(const char * t, int MIN, int MAX) {
    cout << t;
    int time;
    cin >> time;
    while(!checkTime(time)) {
        cout << "\nFeil tidsformat!\n" << t;
        cin >> time;
    }
    return time;
}
// Sjekker om lovlig tidspunkt
bool checkTime(int time) {
    int hh = (time / 10000) % 100; // Moder og henter ut time
    int mm = (time / 100) % 100;   // Moder og henter ut minutt
    int ss = time % 100;           // Moder og henter ut sekund
    
    if(hh > 23 || hh < 1) { // Dersom ulovlig time
        return false;
    }
    else if(mm > 59 || mm < 0) { // Dersom ulovlig minutt
        return false;
    }
    else if(ss > 59 || ss < 0) { // Dersom ulovlig sekund
        return false;
    }
    return true;    // Dersom alt er OK
}

// Sjekker om lovlig dato
bool checkDate(int dato) {
    int aar    = dato / 10000;      // Moder og henter ut aarstall
    int maaned = (dato / 100) % 100;// Moder og henter ut maaned
    int dag    = dato % 100;        // Moder og henter ut dag
    if(aar > 2017 || aar < 2010) {       // Dersom ulovlig aarstall
        cout << "\n\tÅrstall mellom 2010 - 2017!\n"; return false;
    }
    else if(maaned > 12 || maaned < 1) { // Dersom ulovlig maaned
        cout << "\n\tMåned mellom 1 - 12!\n"; return false;
    }
    else if(dag > 31 || dag < 1) {       // Dersom ulovlig dag
        cout << "\n\tDag mellom 1 - 31!\n"; return false;
    } return true;
}

//MENU-FUNCTIONS---------------------------------------------------------------
void MainMenu() {
    cout << "\nListe over valg: " << endl;
    cout << "N - Nasjoner (Registrer ny, endre, faa info)" << endl;
    cout << "D - Deltager (Registrer ny, endre, faa info)" << endl;
    cout << "G - Gren (Registrer ny, endre, faa info)" << endl;
    cout << "O - Ovelse (Registrer ny, endre, faa info)" << endl;
    cout << "M - Medaljeoversikt" << endl;
    cout << "P - Poengoversikt" << endl;
    cout << "X - eXit/Avslutt" << endl;
    cout << endl;
}

void LSubMenu() {
    cout << "\nListe over valg: " << endl;
    cout << "\tS - Skriv deltager/startliste" << endl;
    cout << "\tN - Ny deltager/startliste" << endl;
    cout << "\tE - Endre deltager/startliste" << endl;
    cout << "\tF - Fjerne/slette deltager/startliste" << endl;
    cout << endl;
}

void RSubMenu() {
    cout << "\nListe over valg: " << endl;
    cout << "\tS - Skriv resultatliste" << endl;
    cout << "\tN - Ny resultatliste" << endl;
    cout << "\tF - Fjerne/slette resultatliste" << endl;
    cout << endl;
}
//-----------------------------------------------------------------------------
