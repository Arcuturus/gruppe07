//
//  Nasjoner.cpp
//  Prosjekt
//
//  Created by Anders on 23/03/2017.
//  Copyright © 2017 Anders. All rights reserved.
//

#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#endif

#include "Nasjoner.h"
extern List * deltakere;

void NSubMenu() {
    cout << "\nListe over valg: " << endl;
    cout << "\tN - Registrer ny nasjon" << endl;
    cout << "\tE - Endre nasjon" << endl;
    cout << "\tA - Skriv hoveddataene om alle nasjoner" << endl;
    cout << "\tT - Skriv en nasjons deltakertropp"      << endl;
    cout << "\tS - Skriv alle data om en gitt nasjon" << endl;
	cout << "\tX - Gaa tilbake til hovedmeny" << endl;
    cout << endl;
}
// Nasjonsmeny switch for brukervalg
void nationMenu() {
    char choice = read("Valg? ");
    while (choice != 'X') {
        switch (choice) {
            case 'N':
                newNation();
                nationWriteToFile();
                break;
            case 'E':
                editNation();
                nationWriteToFile();
                break;
            case 'A':
                displayAllNations();
                break;
            case 'T':
                writeContestants();
                break;
            case 'S':
                infoNation();
                break;
            default:
                break;
        }
        choice = read("Valg? ");
    }
	MainMenu(); //Calls main menu after quit
}

//------------------------------START FUNCTIONS--------------------------------

// Display all nations in the list
void displayAllNations() {
    nasjoner->displayList();
}

// Prints all info about the chosen nation
void infoNation() {
    char name[4];                       // Buffer for nasjonsforkortelse
    nationSHORT(name);                  // Les inn fra funksjon
    
    if(nasjoner->inList(name)) {        // Saa lenge nasjon er i listen
        Nasjon * nasj;
        nasj = (Nasjon*)nasjoner->remove(name); // Hent ut i fra listen
        nasj->display();                        // Kall display funksjon
        nasjoner->add(nasj);                    // Legg tilbake i listen
    }
    else {  // Melding til bruker dersom nasjonen ikke finnes i listen
        cout << "\n\tFinner ikke nasjon " << name << "!\n";
    }
    
}

// Edit a nation, such as name and contactinfo
void editNation() {
    char name[4];                           // Buffer for nasjonsforkortelse
    nationSHORT(name);                      // Leser inn fra funksjon
    
    if(nasjoner->inList(name)) {            // Saa lenge nasjon finnes i listen
        Nasjon * nasj;
        nasj = (Nasjon*)nasjoner->remove(name);// Hent ut i fra listen
        nasj->edit();                       // Kall paa edit funksjon
        nasjoner->add(nasj);                // Legg tilbake i listen
    }
    else { // Melding til bruker dersom nasjonen ikke finnes i listen
        cout << "\n\tFinner ikke nasjon " << name << "!\n";
    }
}

// Prints all the associated contestants to this nation
void writeContestants() {
    Deltaker * contestant;
    char name[4];
    nationSHORT(name);
    bool exists = false;
    // Looper paa antall nasjoner i listen
    for(int i = 1; i <= deltakere->noOfElements(); i++) {
        contestant = (Deltaker*)deltakere->removeNo(i); // Hent ut nasjon i
        if(contestant->nationality(name) == true) { // Sjekker om matcher
            exists = true;                          // Bool er lik true
            // Skriver ut alle deltakerene tilhorende denne nasjonen
            cout << "\nALLE DELTAKERE TILHORENDE  " << name << ':';
            contestant->display(); // Kaller paa deltaker sin display funksjon
        }
    } // Dersom ingen match, print informasjon til bruker
    if(!exists) cout << "\nIngen deltakere tilhorende " << name << "!\n";
}

// Creates a new nation / Ask for edit if exists
void newNation() {
    char name[4];
    nationSHORT(name);
    
    if(!nasjoner->inList(name)) {         // Dersom ikke i listen, opprett ny
        nasjoner->add(new Nasjon(name));
    }
    else {
        char valg;                        // Holder brukervalget
        cout << "\n\tNasjonen finnes fra før. Ønsker du å endre på den? ";
        valg = read("J/n: ");             // Les inn brukervalg
        while(valg != 'J' && valg != 'N') { // Saa lenge valget er ugyldig
            valg = read("J/n: ");          // Spor igjen
        } 
        if(valg == 'J') {                 // Dersom ja
            Nasjon * nasj;                // Spor om bruker onsker a endre
            nasj = (Nasjon*)nasjoner->remove(name); // Hent ut fra listen
            nasj->edit();                 // Kall pa nasjon sin edit
            nasjoner->add(nasj);          // Legg tilbake i listen
        }
    }
}

// Read nations from system file
void nationReadFromFile() {
	ifstream file("NASJONER.DTA");              // Opprett innfil
	if (file.good()) {                          // Saa lenge filen er OK
		cout << "Leser fra filen 'NASJONER.DTA' " << endl;
		char buffer[TXT];                       // Buffer for innlesing
		while (!file.eof()) {                   // Saa lenge mer aa lese
			file.getline(buffer, TXT);          // Les linje inn i buffer
			char nasjFork[TXT];                 // Opprett nasj.fork. i buffer
			strcpy(nasjFork, buffer);           // Kopier over til nasj.fork.
			if (strlen(nasjFork) > 1) {         // Sjekk om neste nasj.fork er
				nasjoner->add(new Nasjon(nasjFork, file)); // OK
			}
		}
	}
	else { // Dersom filen ikke finnes, melding til bruker
		cout << "Filen 'NASJONER.DTA' finnes ikke! " << endl;
	}
}

// Print nations to the system file
void nationWriteToFile() {
    ofstream file("NASJONER.DTA");                  // Opprett utfil
    Nasjon * nasjon;
    for(int i = 1; i <= nasjoner->noOfElements(); i++) { // Loop pa nasjoner
        nasjon = (Nasjon*)nasjoner->removeNo(i); // Hent ut nasjon fra listen
        nasjon->skrivTilFil(file);             // Skriv denne nasjonen til fil
        nasjoner->add(nasjon);                  // Legg tilbake i listen
    }
}

//------------------------------END FUNCTIONS----------------------------------
