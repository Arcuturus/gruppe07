#if !defined(__MEDALJER_H)          
#define __MEDALJER_H

#include "Statistikk.h"


class Medalje : public Statistikk {
private:
	char*		 nasjon[MAXNASJONER + 1];
	int	   		 medaljer[MAXNASJONER + 1][4];
	int			 antNasjon;
public:
	Medalje();
	Medalje(char *navn);
	Medalje(ifstream &inn);
	void sjekkForsteGang(ifstream &inn);
	void fjernNasjon(int i);
	void display();
	void readFromFileM();
	void nullerUt();
	void skrivTilFil();
	void sorter();
	void byttPlass(int j);
	~Medalje();
	bool sjekkAlt(int j);
};

#endif