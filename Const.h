#if !defined(__CONST_H)          
#define __CONST_H   

const int TXT			  = 60;                       // MAX stringlenght
const int TLFMIN		  = 10000000;                 // MIN Phone
const int TLFMAX		  = 99999999;                 // MAX Phone
const int MAXOVELS		  = 20;                       // Max no of ovelser
const int NATLEN		  = 4;                        // Max nation short
const int MAXNASJONER	  = 200;                      // Max no of nations
const int MAXDELT         = 40;                       // Max no of contestants

#endif
