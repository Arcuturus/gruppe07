#include "Ovelse.h"
#include "Medaljer.h"
#include "Poeng.h"
#include <string>
#include <cstdio>

extern List* deltakere;

Ovelse::Ovelse(const char * name, int uniq) {
    id = uniq;
    navn = new char[strlen(name) + 1];
    capitalize(strcpy(navn, name));
    klokkeslett = retTime("\tStarttid ovelse(hhmmss): ", 1, 235959);
    dato        = retDate("\tDato ovelse(aaaammdd): ", 20170101, 20171231);
    for(int i = 1; i <= MAXDELT; i++) {
        deltakerListe[i] = nullptr;     // Reset deltakerlista sammen med
        startTime[i] = 0;               // startnummerene og starttidene
        startNum[i]  = 0;
    }
    antDelt = 0, antRes = 0;            // Resetter variablene
}
// Les ovelse fra fil
Ovelse::Ovelse(ifstream &inn) {
	inn >> id; inn.ignore();         // Les inn ovelsesID
	navn = newCharPtr(inn);          // Les inn navn (egen funksjon)
	inn >> klokkeslett; inn.ignore();// Les inn starttid
	inn >> dato; inn.ignore();       // Les inn dato
    antDelt = 0, antRes = 0;         // Reset deltakere og resultat
}
// Edit switch
void Ovelse::edit(char valg) {
    switch (valg) {
        case 'N':
            char name[TXT];
            read("\n\tNytt navn", name, TXT);
            capitalize(strcpy(navn, name));
            break;
        case 'D':
            dato = retDate("\tDato ovelse(aaaammdd): ", 20170101, 20171231);
            break;
        case 'T':
            klokkeslett = retTime("\tStarttid ovelse(hhmmss): ", 1, 235959);
            break;
        default:
            cout << "\n\tIkke et gyldig valg!\n";
            valg = read("\n\tDitt valg");
            break;
    }
}

// Finnes ovelse allerede?
bool Ovelse::finnes(const char * name) {
    return strcmp(navn, name);
}

// Sjekker / Opprettet startlisten
void Ovelse::createStartList() {
    char checkFile[TXT];                        // Filnavn buffer
    strcpy(checkFile, createFileName(".STA"));  // Lag STA filnavn
    
    char * fileName = new char[strlen(checkFile) + 1]; // Filnavn peker
    strcpy(fileName, checkFile);                // Kopier over
 
	ifstream inn(fileName);                  // Lag en innfil
    if(!inn) writeToStartList(fileName);
    else readStartList(inn);
}

void Ovelse::changeStartList() {
    char checkFile[TXT];                               // Filnavn buffer
    strcpy(checkFile, createFileName(".STA"));         // Lag STA filnavn
    
    char * fileName = new char[strlen(checkFile) + 1]; // Filnavn peker
    strcpy(fileName, checkFile);                       // Kopier over

	ifstream inn(fileName);                          // Opprett innfil
    if(inn) {   // Saa lenge det finnes en innfil aa lese fra
        for(int i = 1; i <= antDelt; i++) { // Loop pa ant.delt i listen
            cout << '\t' << deltakerListe[i]->getName() << '\n' // Print delt.
                 << startNum[i] << ' ' << startTime[i] << '\n'; // info
            // Spor om bruker onsker aa endre paa denne deltakeren
            char valg = read("Endre denne deltakeren? (J/n)");
            if(valg == 'J') { // Dersom JA
                // Liste over ting bruker kan endre paa
                cout << "\n\tN - Endre startnummer\n"
                     << "\n\tT - Endre starttid\n"
                     << "\n\tF - Fjern denne deltakeren\n";
                char valg2 = read("Ditt valg"); // Les brukervalg
                switch (valg2) { // Swicth paa brukervalg
                    case 'N': {
                        changeStartNumber(i); // Kall paa funksjon
                        cout << "\n\tStartnummer endret\n"; // OK melding
                    } break;
                    case 'T': { // Endre starttid
                        startTime[i] = retTime("\n\tNy starttid(hhmmss): ", 1, 235959);
                    } break;
                    case 'F': {
                        deleteDeltaker(i); // Kall paa funksjon
                        cout << "\n\tDeltaker fjernet\n";// Melding til bruker
                    } break;
                    default:
                        break;
                }
            }
        }
        updateStartList(fileName); // Oppdater startlisten etter endringer
    }
    else { // Melding til bruker om at startliste ikke eksisterer
        cout << "\n\tFinner ikke startlisten\n";
    }
}
// Endre deltakerens startnummer
void Ovelse::changeStartNumber(int i) {
    int sNum = read("\n\tStartnummer", 100, 999); // Bruker taster startnummer
    for(int i = 1; i <= antDelt; i++) { // Looper paa antall deltakere
        while(startNum[i] == sNum) {    // Sjekker om startnummeret er tatt
            cout << "\n\tFinnes allerede!\n"; // Melding om tatt
            sNum = read("\n\tStartnummer", 100, 999); // Spor igjen
        }
    }
    startNum[i] = sNum; // Alt OK, sett nytt startnummer
}
// Fjern deltaker fra startliste
void Ovelse::deleteDeltaker(int i) {
    for(int j = i; j <= antDelt; j++) {         // Loop igjennom
        deltakerListe[j] = deltakerListe[j+1];  // Sett den fjerede detlakerens
        startTime[j] = startTime[j+1];          // data til aa vare den neste
        startNum[j] = startNum[j+1];            // deltakerens data
    }
    deltakerListe[antDelt] = nullptr;           // Sett siste deltaker til aa
    startNum[antDelt] = 0;                      // vare nullptr, reset alt,
    startTime[antDelt] = 0;                     // og flytt et hakk ned
    --antDelt;
}
// Opdpater startlisten ved endringer (skriv til fil)
void Ovelse::updateStartList(const char * fileName) {
    ofstream file(fileName);
    for(int i = 1; i <= antDelt; i++) {
        file << deltakerListe[i]->getID() << ' '
        << startNum[i] << ' '
        << startTime[i] << '\n';
    }
}

void Ovelse::writeToStartList(const char * fileName) {
    int num = 0;
    ofstream file(fileName); // Dersom ikke, opprett filen
    num = read("\n\tDeltakerens nummer (0 for avslutt)", 0, 9999);
    while(num != 0) { // Saa lenge deltakernummeret ikke er 0
        if(deltakere->inList(num)) { // Sjekk om deltaker finnes
            Deltaker * contestant;   // Holder deltakerobjektet
            contestant = (Deltaker*)deltakere->remove(num); //
            for(int i = 1; i <= antDelt; i++) {        // Sjekk deltnr opp
                while(deltakerListe[i] == contestant) {// mot alle deltakere
                    deltakere->add(contestant);        // i deltlisten
                    // Spor etter nytt deltakernummer
                    num = read("\n\tDeltakerens nummer (0 for avslutt)", 0, 9999);
                    if(deltakere->inList(num)) {
                        contestant = (Deltaker*)deltakere->remove(num);
                    }
                    else contestant = nullptr; // Hvis bruker taster 0
                }
            }
            if(num != 0) { // Saa lenge nummer ikke er 0
                deltakere->add(contestant); // Legg tilbake i listen
                int i = ++antDelt;          // Ok antall deltakere
                deltakerListe[i] = contestant;
                startTime[i] = retTime("\tStarttid (hhmmss): ", 1, 235959);
                changeStartNumber(i);
                file << contestant->getID() << ' '
                << startNum[i] << ' '
                << startTime[i] << '\n';
            }
        }
        else { // Deltakeren finnes ikke blant deltakerene
            cout << "\n\tDenne deltakeren finnes ikke! Prov igjen\n";
        } // Sjekk forst om brukerinput er 0, i motsatt fall: spor om nr
        if(num != 0) num = read("\n\tDeltakerens nummer (0 for avslutt)", 0, 9999);
    }
}

void Ovelse::readStartList(ifstream &inn) {
    int unique;                                  // Holder deltakerid
    while (inn >> unique) { // Saa lenge den klarer aa lese inn et nummer
        ++antDelt;  // Ok antall deltakere, les innhold fra fil
        inn >> startNum[antDelt] >> startTime[antDelt]; inn.ignore();
        Deltaker * contestant;  // Deltakerobjekt
        contestant = (Deltaker*)deltakere->remove(unique); // Hent ut paa nr
        deltakere->add(contestant);                 // Legg tilbake i listen
        deltakerListe[antDelt] = contestant;        // Legg til i startliste
    }
    cout << "\n\tLeste inn fra startlisten\n"; // Melding til bruker
}
// Vis hele startlisten
void Ovelse::printStartList() {
    char checkFile[TXT];
    strcpy(checkFile, createFileName(".STA"));    // Lager filnavn for liste
    char * fileName = new char[strlen(checkFile) + 1];// Setter av nok plass
    strcpy(fileName, checkFile);                  // Kopier over
    
    string str;                                   // Holder en streng
    ifstream file;                                // Innfilen med filnavn
    file.open(fileName);                          // Aapne filen
    while(getline(file, str)) {                   // Saa lenge mer aa lese
        cout << str << '\n';                      // Skriv ut linjen fra fil
    }
}
// Lag filnavn
char * Ovelse::createFileName(const char * extension) { // Tar i mot RES/STA
    string name = "OV" + to_string(id) + extension;     // Bygg opp streng
    char * filename = new char[name.length()+1];          // Sett av nok plass
    strcpy(filename, name.c_str());                     // Kopier over
    return filename;                                    // Returner filnavn
}
// Skriv ut ovelsesinformasjon
void Ovelse::display() {
    cout << id << " | " << navn << '\n'
	     << "Dato: " << dato << ", "
         << "Tidspunkt: " << klokkeslett << '\n'
	     << "Ovelsen har totalt: " << antDelt << " deltagere\n\n";
}
// Skriv ovelse til fil
void Ovelse::writeToFile(ofstream &ut) {
	ut << id << '\n'  << navn  << '\n'
       << klokkeslett << '\n'  << dato << '\n';
}
// Opprett resultatliste for ovelse
void Ovelse::createResultList(moles etter) { // Tar poeng eller tid som param
	antRes = 0; 
	char preName[TXT];                              // Buffer for filnavn
	strcpy(preName, createFileName(".RES"));        // Skap filnavn m/funksjon
	char * fileName = new char[strlen(preName) + 1];// Sett av nok plass
	strcpy(fileName, preName);                      // Kopier over
	ifstream inn(fileName);                         // Opprett innfil
	if (!(inn)) {                                   // Dersom ingen innfil
			if (antDelt != 0) {                     // Dersom det finnes delt.
                if(etter == tid) registrerTid(etter);
                else registrerPoeng(etter);
                for (int i = 1; i <= antDelt; i++) {// Loop igjennom deltakere
                    for(int i = 1; i <= antDelt; i++) {
                        if(results[i] == 0) {
                            deleteDeltaker(i);
                        }
                    }
                }
                writeContestantToFile();          // Skriv til fil
				writeResToFile();                 // Skriv resultatfil
				displayResList(etter);            // Vis den opprettede listen
				Poeng* pong = new Poeng(fileName);// Opprett poeng for res.
				delete pong;                      // Slett etter bruk
				Medalje* med = new Medalje(fileName);// Opprett med. for res.
				delete med;                       // Slett etter bruk
			}
			else {  // Dersom det ikke finnes noen deltakerliste
				cout << "Det finnes ikke deltakerliste for ovelsen! " << endl;
			}
	}
	else {  // Dersom det allerede finnes en resultatliste for ovelse
	cout << "\nDet finnes allerede en resultatliste med navnet " << fileName << endl; 
	} 
}

void Ovelse::registrerTid(moles etter) {
    cout << "\n\tRegistreres som tidel, " // Spor bruker om
         << "hundredel eller tusendel?\n";// hvordan det skal
    char valg = read("\n\tDitt valg('T'idel/'H'undredel/t'U'sendel)");
    switch (valg) {
        case 'T':
            for (int i = 1; i <= antDelt; i++) {
                cout << "\n\tResultat for " << deltakerListe[i]->getName() << '\n';
                results[i] = read("\n\tTid (MMSST)", 0, 179599);
                ++antRes;
                sortResultList(etter);
            }
            break;
        case 'H':
            for (int i = 1; i <= antDelt; i++) {
                cout << "\n\tResultat for " << deltakerListe[i]->getName() << '\n';
                results[i] = read("\n\tTid (MMSSHH)", 0, 1795999);
                ++antRes;
                sortResultList(etter);
            }
            break;
        case 'U':
            for(int i = 1; i <= antDelt; i++) {
                cout << "\n\tResultat for " << deltakerListe[i]->getName() << '\n';
                results[i] = read("\n\tTid (MMSSHHT)", 0, 17959999);
                ++antRes;
                sortResultList(etter);
            }
        default:
            break;
    }
}

void Ovelse::registrerPoeng(moles etter) {
    for(int i = 1; i <= antDelt; i++) {
        cout << "\n\tResultat for " << deltakerListe[i]->getName() << '\n';
        results[i] = read("\n\tPoeng", 0, 150);
        ++antRes;
        sortResultList(etter);
    }
}
// Vis resultatlisten for ovelse
void Ovelse::showResultList(moles etter) { // Poeng eller Tid som parameter
	antRes = 0;
	char checkFile[TXT];
	strcpy(checkFile, createFileName(".RES"));
	char * fileName = new char(strlen(checkFile) +1 );
	strcpy(fileName, checkFile);

	ifstream inn(fileName);

	if (inn) {  // Dersom startlisten finnes
        if(antDelt != 0) {  // Saa lenge det finnes deltakere for ovelsen
			for (int i = 1; i <= antDelt; i++) { // Loop paa ant. delt.
                // Les inn fra fil og skriv ut paa skjerm
				inn.ignore(3); inn.ignore(5); inn >> results[i]; inn.ignore();
				antRes++; 
				
			}
            displayResList(etter); // Display resultatliste
        }
        else { // Dersom startlisten ikke er lest inn ennaa
           cout << "\n\tVennligst les inn startlisten!\n";
        }
	}
	else { // Dersom filen ikke finnes, si ifra til bruker
		cout << "Filen " << fileName << " finnes ikke " << endl; 
	}
}
// Skriv resultater til fil
void Ovelse::writeResToFile() {
	char checkFile[TXT];                            // Buffer for filnavn
	strcpy(checkFile, createFileName(".RES"));      // Skap filnavn v/funksjon
	char * fileName = new char[strlen(checkFile) + 1];  // Sett av nok plass
	strcpy(fileName, checkFile);                    // Kopier over
	ofstream ut(fileName);                          // Opprett utfil med navn
    for(int i = 1; i <= antDelt; i++) {
		ut << deltakerListe[i]->getNat() << '\n'    // Skriv til fil
			<< deltakerListe[i]->getID() << '\n' << results[i];
		if (i != antDelt) ut << '\n';
    }

}
// Skriv deltakere til startlisten
void Ovelse::writeContestantToFile() {
    char checkFile[TXT];                          // Buffer for filnavn
    strcpy(checkFile, createFileName(".STA"));    // Skap filnavn v/funksjon
    char * fileName = new char[strlen(checkFile) + 1];// Sett av nok plass
    strcpy(fileName, checkFile);                  // Kopier over
    
    ofstream ut(fileName);                        // Opprett utfil med navn
    for (int i = 1; i <= antDelt; i++) {          // Loop paa antall deltakere
        ut << deltakerListe[i]->getID() << ' '    // Skriv ut informasjon
           << startNum[i] << ' '
           << startTime[i] << '\n';
    }
}
// Skriv ut resultatliste til bruker
void Ovelse::displayResList(moles etter) {
	for (int i = 1; i <= antDelt; i++) { // Loop paa antall deltakere
		int resultat = results[i];          // Sjekk poeng for delt i
        // Skriv ut all informasjon om deltaker i til bruker
		cout << "\nNavn: "      << deltakerListe[i]->getName() << '\n'
			 << "Nasjon: "      << deltakerListe[i]->getNat() << '\n'
			 << "Nummer: "      << deltakerListe[i]->getID() << '\n'
			 << "Startnummer: " << startNum[i] << '\n'
			 << "Starttid: "
             << (startTime[i] / 10000) % 100 << ':' // Mod starttid for penere
             << (startTime[i] / 100) % 100 << ':';  // utskrift
        if(startTime[i] % 100 < 10) {
            cout << startTime[i] % 100 << '0';
        }
        else {
            cout << startTime[i] % 100 << '\n';
        }
        if(etter == tid) {
            cout << "\nTid:\t" << (resultat / 1000) << ':'
                 << (resultat / 1000) % 100 << ':';
            if(resultat < 179599) {
                cout << resultat % 100 / 10 << '\n'; // tidel
            }
            if(resultat > 179599) {
                cout << resultat % 100 << '\n'; // hundredel/tusendel
            }
                 
        }
        else {
            cout << "\nPoeng:\t" << resultat << '\n';
        }
	}
}
																					// Sorters the resultlist after points or time
void Ovelse::sortResultList(moles etter) {
	for (int i = antRes; i >= 1; i--) {												//Starts at the back and goes downward
		if (i != 1) {																//as long as it's not index 1
			if (!etter) {															//if results is not measured in time
				if (results[i] > results[i - 1]) {									//if example index nr.3 is greater than index nr.2
					sort(i);														//sort them
				}
			}
			else {  
				if (results[i] < results[i - 1]) {									//if example index nr.3 is less than index nr.2
					sort(i);														//Sort them
				}
			}
		}
	}
}

void Ovelse::sort(int i) {															 //Sorts result and startlist, and startnr and starttime arrays
	int resultTemp, startTimeTemp, startNumTemp;									 //Stores away int's to change value
	Deltaker * delTemp;																 //Stores away contestant pointer
    
	resultTemp = results[i];														 //Sets example temp to index nr.3
    results[i] = results[i - 1];													 //sets index nr. 3 to index nr.2
    results[i - 1] = resultTemp;													 //sets index nr.2 to temp 
																					 //Then they are swapped, and sorted
	delTemp = deltakerListe[i];
    deltakerListe[i] = deltakerListe[i - 1];
    deltakerListe[i - 1] = delTemp;
    
    startTimeTemp = startTime[i];
    startTime[i] = startTime[i - 1];
    startTime[i - 1] = startTimeTemp;
    
    startNumTemp = startNum[i];
	startNum[i] = startNum[i - 1];
	startNum[i - 1] = startNumTemp;
}
// Fjern resultatfilen
void Ovelse::deleteResultList() {
	char checkFile[TXT];                            // Filnavn buffer
	strcpy(checkFile, createFileName(".RES"));      // Lag RES filnavnet
	char * fileName = new char[strlen(checkFile) + 1];  // Filnavn peker
	strcpy(fileName, checkFile);                    // Kopier over til peker

	if (ifstream(fileName)) {                 // Dersom finnes, prov aa slett
        ifstream fil(fileName);
        Poeng * po = new Poeng(fil);
        delete po;
		ifstream fil2(fileName);
        Medalje * med = new Medalje(fil2);
        delete med;
		fil.close(); fil2.close();
		if (remove(fileName) != 0) {          // Sjekk om klarer aa slette
			perror("Error deleting file");    // Ikke slettet, feilmelding
		}
		else                                  // Klarte aa slette
			puts("File successfully deleted");// Melding til bruker
	}
	else {
		cout << "Filen finnes ikke!"; 
	}
}

void Ovelse::deleteStartList() {
    char checkFile[TXT];                             // Filnavn buffer
    strcpy(checkFile, createFileName(".RES"));       // Lag RES filnavnet
    char * resFileName = new char[strlen(checkFile) + 1];// Lag filnavn peker
    strcpy(resFileName, checkFile);                  // Kopier over
    
    // Sjekk om resultatfilen finnes
    if (ifstream(resFileName)) {
        cout << "\n\tKan ikke slette startfil da resultatfil finnes!\n";
    }
    else { // Dersom RES filen finnes
        strcpy(checkFile, createFileName(".STA"));        // Lag STA filnavnet
        char * staFileName = new char[strlen(checkFile) + 1]; // Lag filnavn peker
        strcpy(staFileName, checkFile);                   // Kopier over
        
        // Check if startlist exists
        if(ifstream(staFileName)) {         // Dersom finnes
            if (remove(staFileName) != 0) { // Sjekk om den klarer aa slette
                perror("\n\tError deleting file\n"); // Feilmelding
            }
            else // Filen ble slettet, melding til bruker
                puts("\n\tFile successfully deleted\n");
        }
    }
}

bool Ovelse::removeSelf() {
    
    char checkFile[TXT];
    
    strcpy(checkFile, createFileName(".STA"));   // Lager filnavnet til
    char * staFile = new char[strlen(checkFile) + 1];// startfilen
    strcpy(staFile, checkFile);
    
    strcpy(checkFile, createFileName(".RES"));   // Lager filnavnet til
    char * resFile = new char[strlen(checkFile) + 1];// resultatfilen
    strcpy(resFile, checkFile);
    
    if(ifstream(staFile)) { // Dersom startfil finnes
        if(!remove(staFile)) return false;  // Forsok aa slette denne
    }
    if(ifstream(resFile)) { // Dersom resultatliste finnes
        ifstream fil(resFile);
        Poeng * po = new Poeng(fil);
        delete po;
        ifstream fil2(resFile);
        Medalje * med = new Medalje(fil2);
        delete med;
        fil.close(); fil2.close();
        if(!remove(resFile)) return false;  // Forsok aa slette denne
    }
    delete this;    // Kall destructor
    return true;    // return
}

Ovelse::~Ovelse() {
    delete[] navn;                      // Slett navnepeker
    for(int i = 1; i <= MAXDELT; i++) { // Tom alle starttider/-nummer
        startTime[i] = 0;
        startNum[i] = 0;
    }
    for(int i = 1; i < MAXDELT; i++) { // Tommer deltakerlisten
        delete[] deltakerListe[i];     // Slett deltakerlistepeker
    }
}


void Ovelse::nullStill() {
	for (int i = 0; i <= MAXDELT; i++) {
		results[i] = 0;
		startNum[i] = 0;
		startTime[i] = 0; 
		deltakerListe[i] = nullptr;
	}
}
