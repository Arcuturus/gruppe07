#include "Deltaker.h"

Deltaker::Deltaker(int num) : NumElement(num) {
	addNation();									//Adds nation for the user
	name = newCharPtr("Deltakerens navn? ");
	gender();										//Decides the gender of contestant
	vekt = read("Hva er vekten til deltakeren? ", 45, 150);
	hoyde = read("Hva er hoyden til deltakeren? ", 1.20F, 2.60F);
	nasjon->antDeltInc();							//increases antDeltakere in Nasjon
}

Deltaker::Deltaker(ifstream & inn, int nr) : NumElement(nr) { //Sends uniqeID up hierarchy
	name = newCharPtr(inn);									//reads name from file
	inn >> vekt; inn.ignore();
	inn >> hoyde; inn.ignore();

	char buffer[TXT];				//Stores char array	
	inn.getline(buffer, TXT);		//Get text from file
	if (buffer[0] == 'K') {			//if first index = K
		kjonn = dame;				//kjonn = dame
	}
	else {
		kjonn = herre;				//else kjonn = herre 
	}
	
	char * nasjonNavn = newCharPtr(inn);						//reads name of nation
		Nasjon * nasj;											//creates temp nation 
		nasj = (Nasjon*)nasjoner->remove(nasjonNavn);			//removes the nation from the list, sets it to temp
		nasjon = nasj;											//Contestant nation = temp (nasj) 
		nasjoner->add(nasj);									//Add the nation back in the list
}

void Deltaker::skrivTilFil(ofstream & ut)
{
	ut << number << endl;  
	ut << name << endl;
	ut << vekt << endl;
	ut << hoyde << endl;
	(kjonn == 1) ? (ut << "Mann" << endl) : (ut << "Kvinne" << endl);
	nasjon->outName(ut);															//Prints nation shortname ex. "NOR"
}

void Deltaker::edit()
{
	editMenu(); 
	char choice = read("Valg? ");
	while (choice != 'X') {
		switch (choice) {
		case 'C': {
			addNation();
			cout << "\nNasjon er endret!" << endl; 
		}																			//Change nation
			break;
		case 'N': {
			name = newCharPtr("Deltakerens navn? ");								//edit name
			cout << "\nNavn er endret!" << endl;
			}
			break;
		case 'K': gender();															//edit gender
			cout << "\nKjonn er endret!" << endl;
			break; 
		case 'V': {
			vekt = read("Hva er vekten til deltakeren? ", 45, 150);					//edit weight
			cout << "\nVekt er endret!" << endl;
		}
			break; 
		case 'H': {
			hoyde = read("Hva er hoyden til deltakeren? ", 1.20F, 2.60F);			//edit height
			cout << "\nHoyden er endret!" << endl;
		}
			break; 
		default: 
			editMenu();
			break; 
		}
		choice = read("Valg? ");													//Choose a new option
	};

	DSubMenu(); 
}

void Deltaker::gender() {															//Decides the gender of contestant
	char k = read("Hvilket kjonn er deltakeren (K(vinne)/M(ann))? ");		
	switch (k) {
	case 'K': kjonn = dame;
		break;
	case 'M': kjonn = herre;
		break;
	default: k = read("Hvilket kjonn er deltakeren (K(vinne)/M(ann))? ");
		break;
	}
}

bool Deltaker::nationality(const char * name) {
    return (!strcmp(name, nasjon->getName()));
}

void Deltaker::addNation() {

	char name[4];																	//Stores short name of nation
	nationSHORT(name);																//Gets the name from user

		if (!nasjoner->inList(name)) {												//if nation doesn't exist
			char choice = read("Nasjonen finnes ikke, vil du opprette ny (J/N)? ");
			while (choice != 'J' && choice != 'N') {								//Has to be J or N
				choice = read("Nasjonen finnes ikke, vil du opprette ny (J/N)? ");
			}
            if(choice == 'J') {														//If J, create new nation
					nasjon = new Nasjon(name);
					nasjoner->add(nasjon);
					nationWriteToFile(); 
					
				}
			 
				else {
					cout << "Deltaker ma ha en tildelt nasjon!" << endl;			//If N, maybe a typo, call itself, start over
                    addNation();  
			}

		}
		else {
			Nasjon * nasj;						   //If exists
			nasj = (Nasjon*)nasjoner->remove(name);//remove from list
			nasjon = nasj;		//Set the contestant's nation to the existing
			nasjoner->add(nasj);				   //Add nation back to the list
		}

	
}

void Deltaker::display()
{
	cout << "\n\nDeltakerens navn: " << name << endl
		<< "Vekt: " << vekt << "kg" << endl
		<< "Hoyde: " << hoyde << endl
		<< "Kjonn: "; 
	(kjonn == 1) ? cout << "Mann" : cout << "Kvinne"; 
	nasjon->display();							//Display contestant's nation
	cout << endl; 
}

Deltaker::~Deltaker() {
    delete nasjon;
    delete name;
}

