#include "Poeng.h"
#include <cstdio>

//kj�res n�r du trykker 'P' leser fra fil kj�rer display
Poeng::Poeng() {
	ifstream inn("POENG.DTA");
	
	if (inn) {
		readFromFileP();
	}
	else {
		cout << "\nFant ikke filen 'POENG.DTA'" << endl;
		nullstill();
	}	
	displayP();
}

//kj�res n�r du legger til resultat liste
Poeng :: Poeng(char* navn){
	char* nasjonfork;
	int points = 0, pla = 0;
	int trash;

	antall = 0;
	nullstill();
	readFromFileP();
	ifstream inn(navn);
    
	if (inn && antall !=0) {				//hvis den finner fila og det
		while(!inn.eof() && pla < 7){		//looper til end of file
            nasjonfork = newCharPtr(inn);
            inn >> trash; inn.ignore();			//henter f�rste deltager
            inn >> trash; inn.ignore();
            ++pla;								//eller 7 ganger
			if (pla == 1)points = 7;
			else if (pla == 2)points = 5;
			else if (pla == 3)points = 4;
			else if (pla == 4)points = 3;
			else if (pla == 5)points = 2;
			else if (pla == 6)points = 1;
            
            bool finnes = false;

					//sjekker hvor den finnes i arrayet og legger til poeng
			for (int i = 1; i <= antall; i++) {
				if (!strcmp(nasjonfork, plass[i])) {
                    finnes = true;
					poenger[i] += points;
				}
			}		//hvis den ikke finnes legges det til ny nasjon
			if (!finnes) {
				plass[++antall] = nasjonfork;
				poenger[antall] += points;
			}		//leser inn p� nytt
//			nasjonfork = newCharPtr(inn); 
//			inn >> trash; inn.ignore();
//			inn >> trash; inn.ignore();
//			++pla;
		}
	}					//kj�res kunn f�rste gang medaljer opprettes
	else if (inn){		//henter inn f�rste deltager
		sjekkForsteGang(inn);
	}
	else cout << "\nFant ikke filen " << navn << endl;
	sorter();			//sorterer etter poeng
	writeToFileP();		//f�r det skrives til fil
}

//kj�res n�r du sletter resultatliste
Poeng::Poeng(ifstream &inn) {
	char* nasjonfork;
	int points = 0, pla = 0;
	int trash;

	nullstill();			//nullsiller og leser fra fil
	readFromFileP();

	if (inn && antall != 0) {					//sjekker om den finner fila
		while (!inn.eof() && pla <7) {			//looper til eof eller 7
            nasjonfork = newCharPtr(inn);			//og at antall er mer en 1
            inn >> trash; inn.ignore();				//leser inn f�rste deltager
            inn >> trash; inn.ignore();
            ++pla;
			if (pla == 1)points = 7;
			else if (pla == 2)points = 5;
			else if (pla == 3)points = 4;
			else if (pla == 4)points = 3;
			else if (pla == 5)points = 2;
			else if (pla == 6)points = 1;

					//looper til den finner nasjon
			for (int i = 1; i <= antall; i++) {
				if (!strcmp(nasjonfork, plass[i])) {
					poenger[i] -= points;
					if (!poenger[i]) {		 //vis poeng bli 0 slettes nasjon
						fjernNasjon(i);		 //fra lista
					}
				}
			}
//			nasjonfork = newCharPtr(inn); 
//			inn >> trash; inn.ignore();
//			inn >> trash; inn.ignore();
//			++pla;
		}
	}
	sorter();			//sorterer poeng
	writeToFileP();		//f�r det skrives til fil
	if (!antall) {
		remove("POENG.DTA");  //sletter filen om det ikke er mer
	}
}

//fjerner nasjon nr i, flytter arrayet, nullstiller skuff 'antall' og --antall
void Poeng::fjernNasjon(int i) {
	for (int j = i; j < antall; j++) {
		plass[j] = plass[j + 1];
		poenger[j] = poenger[j + 1];
	}
	plass[antall] = nullptr;
	poenger[antall] = 0;
	--antall;
}

void Poeng::sjekkForsteGang(ifstream &inn) {
	char *navn;
	int pla = 0;
	int trash, points = 0;
	bool finnes;

	plass[++antall]  = newCharPtr(inn);
	poenger[antall] += 7;
	inn >> trash; inn.ignore();
	inn >> trash; inn.ignore();
	++pla;
	
	while (!inn.eof() && pla < 7) {		//looper til eof eller 7 ganger
		navn = newCharPtr(inn);
		inn >> trash; inn.ignore();
		inn >> trash; inn.ignore();
		++pla;
		if (navn) {
			if (pla == 2)points = 5;
			else if (pla == 3)points = 4;
			else if (pla == 4)points = 3;
			else if (pla == 5)points = 2;
			else if (pla == 6)points = 1;

			finnes = false;

			for (int i = 1; i <= antall; i++) {
				if (!strcmp(plass[i], navn)) {
					finnes = true;
					poenger[i] += points;
				}
			}
			if (!finnes) {
				plass[++antall] = navn;
				poenger[antall] += points;
			}
		}
	}
}

void Poeng::readFromFileP() {
	ifstream inn("POENG.DTA");
	char* nasjonfork;

	if (inn) {				//henter antall og looper p�det
        inn >> antall; inn.ignore();
		for (int i = 1; i <= antall; i++) {
			nasjonfork = newCharPtr(inn);
			plass[i] = nasjonfork;
			inn >> poenger[i]; inn.ignore();
		}
	}
}

void Poeng :: writeToFileP() {
	ofstream ut("POENG.DTA");

	ut << antall << endl;
	for (int i = 1; i <= antall; i++) {
		ut << plass[i] << endl << poenger[i] << endl;
	}
}

void Poeng::displayP() {
	for (int i = 1; i <= antall; i++) {
		cout << "\nPlass: " << i << "\tEr: " << plass[i];
		cout << "\nOg har totalt: " << poenger[i] << " poeng\n";
	}
}

void Poeng::sorter() {
	int j, temper;
	char * temp;
	
	for (int i = 1; i <= antall;i++) {
		j = i;
		while (poenger[j] > poenger[j - 1] && j > 1) {
			temper = poenger[j];
			poenger[j] = poenger[j - 1];
			poenger[j - 1] = temper;
			temp = plass[j];
			plass[j] = plass[j - 1];
			plass[j - 1] = temp;
			--j;
		}
	}
}

void Poeng::nullstill() {
	antall = 0;
	for (int i = 1; i <= MAXNASJONER; i++) {
		plass[i] = nullptr;
		poenger[i] = 0;
	}
}

Poeng::~Poeng() {
	for (int i = 1; i <= antall; i++) {
		plass[i]=nullptr;
	}
}
