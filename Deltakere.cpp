//
//  Deltakere.cpp
//  Prosjekt
//
//  Created by Anders on 23/03/2017.
//  Copyright © 2017 Anders. All rights reserved.
//

#include "Deltakere.h"

void DSubMenu() {
	cout << "\nListe over valg: " << endl;
	cout << "\tN - Registrer ny deltager" << endl;
	cout << "\tE - Endre deltager" << endl;
	cout << "\tA - Skriv hoveddataene om alle deltagere" << endl;
	cout << "\tS - Skriv alle data om en gitt deltager" << endl;
	cout << "\tX - Gaa tibake til hovedmeny" << endl; 
	cout << endl;
}

void contestantMenu() {
	char choice; 
	choice = read("Valg? ");
		while (choice != 'X') {
			switch (choice) {
			case 'N':
				newContestant(); 
				deltakerSkrivTilFil();
				break; 
			case 'E': 
				editContestant(); 
				deltakerSkrivTilFil();
				break; 
			case 'A':
				printAllData(); 
				break; 
			case 'S': 
				printSingleData(); 
				break; 
			default: DSubMenu(); 
				break; 
			}
			DSubMenu();
			choice = read("Valg? ");
	}
		MainMenu(); 

}

void newContestant() {														 //Creates new contestant
	int uniqeNr;															 //Store contestant's uniqe ID
	uniqeNr = read("Hva er deltakerens unike nummer? ", 1000, 9999); 
	if (!deltakere->inList(uniqeNr)) {										 //If contestant doesn't exist
		deltakere->add(new Deltaker(uniqeNr));
	}
	else {
		cout << "Deltakeren finnes allerede" << endl;						 //If contestant exists
	}

}

void editContestant() {
	int uniqeNr = read("Hva er deltakerens unike nummer? ", 1000, 9999);	//Who is contestant
	if (deltakere->inList(uniqeNr)) {										//If contestant exists
		Deltaker * deltaker;											    //create temp contestant
		deltaker = (Deltaker*)deltakere->remove(uniqeNr);					//Get contestant from list
		deltaker->edit();													//Edit specific contestant
		deltakere->add(deltaker); 
	}
	else {
		cout << "Deltakeren finnes ikke! " << endl; 
	}
}

void printAllData() {
	deltakere->displayList(); 
}

void printSingleData() {
	int uniqeNr = read("Hva er deltakerens unike nummer? ", 1000, 9999);	//Who is contestant
	if (deltakere->inList(uniqeNr)) {										//If contestant exists
		Deltaker * deltaker;											    //create temp contestant
		deltaker = (Deltaker*)deltakere->remove(uniqeNr);					//Get contestant from list
		deltaker->display();												//Edit specific contestant
		deltakere->add(deltaker);
	}
	else {
		cout << "\nDeltakeren finnes ikke!" << endl; 
	}
}

void deltakerSkrivTilFil() {
		ofstream ut("DELTAKERE.DTA");							   //creates a file
		Deltaker* deltaker;										   //Creates a temp-element
		int j = deltakere->noOfElements();						   //j = number of contestants
		ut << j << endl;										   //Print number of contestants to file
		for (int i = 1; i <= j; i++) {							   //loops through all contestants
			deltaker = (Deltaker*)deltakere->removeNo(i);		   //removes every contestant
			deltaker->skrivTilFil(ut);							   //prints every contestants data
			deltakere->add(deltaker);							   //Puts the contestant back in list
		}
}

void deltakerLesFraFil() {
		ifstream inn("DELTAKERE.DTA");							    //Opens the file
		int elements;												//Stores number of elements
		int uniqeID;												//stores the uniqeID of CONTESTANT

		if (inn) {													//checks that file exists
			cout << "Leser fra filen 'DELTAKERE.DTA' " << endl;
			inn >> elements; inn.ignore();							//get the number of elements
			inn >> uniqeID; inn.ignore();
			Deltaker * deltaker;						 			//Create a temp contestant-element
			for (int i = 1; i <= elements; i++) {					//loop through nr.of elements
				deltaker = new Deltaker(inn, uniqeID);				//create new contestant-element
				inn >> uniqeID; inn.ignore();
				deltakere->add(deltaker);							//Add contestant element to list
			}
		}
		else (cout << "Filen 'DELTAKERE.DTA' finnes ikke! " << endl); 
}

void editMenu() {
	cout << "\n\tHva ønsker du å endre på? "
		<< "\n\tC - Nasjon"
		<< "\n\tN - Navn"
		<< "\n\tK - Kjonn"
		<< "\n\tV - Vekt"
		<< "\n\tH - Hoyde"
		<< "\n\tX - Avslutt"
		<< endl; 
}
